﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3lab
{
    public void PlantListToDictionaryAndSomeOperations()
    {
        // Arrange
        var plants = new List<Plant>(PlantMock.Plants);


        // Act
        var plantDictionary = plants.ToDictionary(plant => plant.Id, plant => plant);
        var result = plantDictionary.Reverse().AsParallel().WithDegreeOfParallelism(2)
            .Where(element => element.Value.Cost > 30000);


        // Assert
        Assert.Equal(result.Count(), 6);
    }
    var plantSortedListFirstElementCost = plants.ToSortedList(plant => plant.Cost).First().Key;



    [Fact]
    public void CopyAsAnonymousClass_CountAnonymous_ShouldBeSameAsListUsedForTest()
    {
        // Arrange
        var plantsCount = PlantMock.Plants.Count;

        // Act
        var plants = PlantMock.Plants.Select(plant => new { plant.Id, plant.Cost });

        // Assert
        Assert.Equal(plants.Count(), plantsCount);
    }


    new Plant(PlantMock.Models[(new Random()).Next(PlantMock.Models.Count())].Id, 29000, "Red");
}


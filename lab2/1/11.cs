﻿using System;
using System.Diagnostics;

namespace _1
{
    public interface Interface
    {
        void DoSmth();
    }

    public struct Struct1 : Interface
    {
        public void DoSmth()
        {
            Console.WriteLine("Struct1 - DoSmth");
        }

        public static implicit operator Struct1(Struct2 v)
        {
            throw new NotImplementedException();
        }
    }

    public struct Struct2 : Interface
    {
        public void DoSmth()
        {
            Console.WriteLine("Struct2 - DoSmth");
        }
    }

    public struct Struct3 : Interface
    {
        public void DoSmth()
        {
            Console.WriteLine("Struct3 - DoSmth");
        }
    }

    public struct Struct4 : Interface
    {
        public void DoSmth()
        {
            Console.WriteLine("Struct4 - DoSmth");
        }
    }

    class Program
    {
        static void Main()
        {
            Struct1 polymorphicObject = new Struct2();
            polymorphicObject.DoSmth();

            Interface interfaceObject = new Struct2();
            interfaceObject.DoSmth();

            Type type = typeof(Struct2);
            object reflectionObject = Activator.CreateInstance(type);
            type.GetMethod("DoSmth").Invoke(reflectionObject, null);

            BenchmarkPerformance();
        }

        static void BenchmarkPerformance()
        {
            const int iterations = 1000000;

            Stopwatch stopwatch1 = new Stopwatch();
            stopwatch1.Start();

            for (int i = 0; i < iterations; i++)
            {
                Struct1 polymorphicObject = new Struct2();
                polymorphicObject.DoSmth();
            }

            stopwatch1.Stop();
            Console.WriteLine("Віртуальні методи: " + stopwatch1.ElapsedMilliseconds + " мс");

            Stopwatch stopwatch2 = new Stopwatch();
            stopwatch2.Start();

            for (int i = 0; i < iterations; i++)
            {
                Interface interfaceObject = new Struct2();
                interfaceObject.DoSmth();
            }

            stopwatch2.Stop();
            Console.WriteLine("Інтерфейси: " + stopwatch2.ElapsedMilliseconds + " мс");

            Stopwatch stopwatch3 = new Stopwatch();
            stopwatch3.Start();

            Type reflectionType = typeof(Struct2);
            for (int i = 0; i < iterations; i++)
            {
                object reflectionObject = Activator.CreateInstance(reflectionType);
                reflectionType.GetMethod("DoSmth").Invoke(reflectionObject, null);
            }

            stopwatch3.Stop();
            Console.WriteLine("Рефлексія: " + stopwatch3.ElapsedMilliseconds + " мс");
        }
    }
}

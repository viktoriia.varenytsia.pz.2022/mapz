﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class Pet
    {
        private string _name;
        private string _breed;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Breed
        {
            get { return _breed; }
            set { _breed = value; }
        }

        private class Color
        {
            private string _mainColor;
            private string _secondaryColor;
        }

        public Pet()
        {
            _name = string.Empty;
        }

        public void Print()
        {
            Console.WriteLine($"Name: {_name}, Breed: {_breed}");
        }
    }

    struct Сar
    {
        private string _color;

        public Car(string color)
        {
            _color = color;
        }

        public void Display()
        {
            Console.WriteLine($"Car: color {_color}");
        }
    }

    interface Dog
    {
        void Eat(string food);
        void Chill(int time);
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Fact]
public void GroupByOrderBy_PlantCostComparer_ShouldBeEqualToOrderByField()
{
    // Arrange
    var groupedWithoutComparator = PlantMock.Plants.Select(plant => plant.Cost).OrderBy(cost => cost);

    // Act
    var orderedEnumerable = PlantMock.Plants.OrderBy(_ => _, new PlantCostComparer());

    // Assert
    Assert.Equal(orderedEnumerable.Select(plant => plant.Cost), groupedWithoutComparator);

    PlantMock.Models.Where(plant => plant.Id == plants.First().ModelNumber).First().Color;

}




public static class ListExtension
{

    public static SortedList<TKey, TValue> ToSortedList<TKey, TValue>
        (this List<TValue> list, Func<TValue, TKey> keySelector) where TKey : notnull
    {
        var distinctItems = list.DistinctBy(keySelector).ToList();
        var sortedDictionary = distinctItems.ToDictionary(keySelector, x => x);
        return new SortedList<TKey, TValue>(sortedDictionary);
    }
}


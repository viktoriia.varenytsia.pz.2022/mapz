﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3lab
{
    public class PlantBrandComparer : IComparer<Plant>
    {
        public int Compare(Plant? plantOne, Plant? plantTwo)
        {
            if (plantOne == null || plantTwo == null)
            {
                throw new NullReferenceException();
            }
            var plantOneBrand = PlantMock.Models.FirstOrDefault(model => model.Id == plantOne.ModelNumber)?.Brand;
            var plantTwoBrand = PlantMock.Models.FirstOrDefault(model => model.Id == plantTwo.ModelNumber)?.Brand;


            return string.Compare(plantOneBrand, plantTwoBrand);
        }
    }


    [Fact]
    public void ConvertToArray_PlantArray_ShouldBeEqualToPlantList()
    {
        // Arrange
        var plantList = new List<Plant>(PlantMock.Plants);


        // Act
        var plantArray = plantList.ToArray();


        // Assert
        Assert.Equal(plantList, plantArray);
    }


    [Fact]
    public void ModelsListSortByModelName_LastElement_ShouldBeEqualToExcepted()
    {
        // Arrange
        var testExpected = "XPS";
        var modelsList = PlantMock.Models;


        // Act
        modelsList.Sort((x, y) => x == null ? (y == null ? 0 : -1) : (y == null ? 1 : x.ModelName.CompareTo(y.ModelName)));
        var lastModel = modelsList.Last();

        // Assert
        Assert.Equal(lastModel.ModelName, testExpected);
    }

}

﻿using System;
using System.Diagnostics;

public interface IMyInterface
{
    void DoSmth();
}

public class Class1 : IMyInterface
{
    public virtual void DoSmth()
    {
        Console.WriteLine("Class1 - DoSmth");
    }
}

public class Class2 : Class1
{
    public override void DoSmth()
    {
        Console.WriteLine("Class2 - DoSmth");
    }
}

public class Class3 : Class1
{
    public override void DoSmth()
    {
        Console.WriteLine("Class3 - DoSmth");
    }
}

class Program
{
    static void Main()
    {
        Class1 polymorphicObject = new Class2();
        polymorphicObject.DoSmth();

        IMyInterface interfaceObject = new Class2();
        interfaceObject.DoSmth();

        Type type = typeof(Class2);
        object reflectionObject = Activator.CreateInstance(type);
        type.GetMethod("DoSmth").Invoke(reflectionObject, null);

        BenchmarkPerformance();
    }

    static void BenchmarkPerformance()
    {
        const int iterations = 1000000;

        Stopwatch stopwatch1 = new Stopwatch();
        stopwatch1.Start();

        for (int i = 0; i < iterations; i++)
        {
            Class1 polymorphicObject = new Class2();
            polymorphicObject.DoSmth();
        }

        stopwatch1.Stop();
        Console.WriteLine("Віртуальні методи: " + stopwatch1.ElapsedMilliseconds + " мс");

        Stopwatch stopwatch2 = new Stopwatch();
        stopwatch2.Start();

        for (int i = 0; i < iterations; i++)
        {
            IMyInterface interfaceObject = new Class2();
            interfaceObject.DoSmth();
        }

        stopwatch2.Stop();
        Console.WriteLine("Інтерфейси: " + stopwatch2.ElapsedMilliseconds + " мс");

        Stopwatch stopwatch3 = new Stopwatch();
        stopwatch3.Start();

        Type reflectionType = typeof(Class2);
        for (int i = 0; i < iterations; i++)
        {
            object reflectionObject = Activator.CreateInstance(reflectionType);
            reflectionType.GetMethod("DoSmth").Invoke(reflectionObject, null);
        }

        stopwatch3.Stop();
        Console.WriteLine("Рефлексія: " + stopwatch3.ElapsedMilliseconds + " мс");
    }
}

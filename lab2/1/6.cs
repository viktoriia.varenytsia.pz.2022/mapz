﻿using System;

public class Class
{
    public void Something()
    {
        Console.WriteLine("Base class");
    }
}

public interface Interface1
{
    void Function1();
}

public interface Interface2
{
    void Function2();
}

public class Class2 : Class, Interface1, Interface2
{
    public void Function1()
    {
        Console.WriteLine("Interface 1 Function1");
    }

    public void Function2()
    {
        Console.WriteLine("Interface 2 Function2");
    }
}

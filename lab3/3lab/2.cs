﻿using System;
using System.Linq;

public class PlantMock
{
    public static readonly Species[] SpeciesList = new Species[]
    {
        new Species("Rose", "Rosa", true),
        new Species("Oak", "Quercus", false),
        new Species("Sunflower", "Helianthus", true),
        new Species("Tulip", "Tulipa", true),
        new Species("Pine", "Pinus", false),
        new Species("Daisy", "Bellis", true),
        new Species("Lily", "Lilium", true)
    };

    public static Plant[] Plants = GeneratePlants();

    private static Plant[] GeneratePlants()
    {
        var random = new Random();
        return Enumerable.Range(1, 10)
            .Select(_ =>
            {
                var speciesIndex = random.Next(SpeciesList.Length);
                var cost = random.Next(100, 1000);
                var colorIndex = random.Next(4); // Assuming Color is represented by a string
                var color = GetColorName(colorIndex);
                return new Plant(SpeciesList[speciesIndex].Name, SpeciesList[speciesIndex].SpeciesName, cost, color);
            })
            .ToArray();
    }

    public static void PrintAll()
    {
        Console.WriteLine("{0,-12} {1,-20} {2,-10} {3,-12}", "Name", "Species", "Cost", "Color");
        Console.WriteLine("---------------------------------------------------------------");

        foreach (var plant in Plants)
        {
            Console.WriteLine(String.Format("{0,-12} {1,-20} {2,-10} {3,-12}",
                plant.Name ?? "Unknown",
                plant.Species ?? "Unknown",
                plant.Cost.ToString(),
                plant.Color));
        }
    }

    private static string GetColorName(int index)
    {
        switch (index)
        {
            case 0:
                return "Red";
            case 1:
                return "Green";
            case 2:
                return "Blue";
            default:
                return "Yellow";
        }
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        PlantMock.PrintAll();
    }
}

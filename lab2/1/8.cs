﻿using System;

namespace lab2_9
{
    public class Cat
    {
        public double _weight { get; }

        public Cat(double weight)
        {
            _weight = weight;
        }

        // Явний оператор приведення від Cat до double
        public static explicit operator double(Cat cat)
        {
            return cat._weight;
        }

        // Неявний оператор приведення від double до Cat
        public static implicit operator Cat(double weight)
        {
            return new Cat(weight);
        }
    }

    class Program
    {
        static void Main()
        {
            Cat cat1 = new Cat(10);
            // Викликає явний оператор приведення
            double weight1 = (double)cat1;
            // Виведе: 10
            Console.WriteLine(weight1);

            double weight2 = 25;
            // Викликає неявний оператор приведення
            Cat cat2 = weight2;
            // Виведе: 25
            Console.WriteLine(cat2._weight);
        }
    }
}

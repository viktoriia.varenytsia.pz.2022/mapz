﻿using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;

public class PlantTests
{
    [Fact]
    public void PlantListToDictionary_PlantDictionary_ShouldBeTypeOfGuidAndPlant()
    {
        // Arrange
        var plants = new List<Plant>(PlantMock.Plants);

        // Act
        var plantDictionary = plants.ToDictionary(plant => plant.Id, plant => plant);

        // Assert
        Assert.IsType<Dictionary<Guid, Plant>>(plantDictionary);
    }

    [Fact]
    public void PlantListToDictionaryBySpecies_PlantDictionaryBySpecies_ShouldBeTypeOfGuidAndPlantList()
    {
        // Arrange
        var plants = new List<Plant>(PlantMock.Plants);

        // Act
        var plantDictionaryBySpecies = plants.GroupBy(p => p.Species).ToDictionary(g => g.Key, g => g.ToList());

        // Assert
        Assert.IsType<Dictionary<string, List<Plant>>>(plantDictionaryBySpecies);
    }
}

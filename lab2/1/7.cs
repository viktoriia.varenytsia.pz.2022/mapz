﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 namespace Lab2_8
    {
        public class Class
        {
            private static int staticField = InitStaticField();
            private int field;

            static Class()
            {
                Console.WriteLine("Static constructor");
            }

            public Class()
            {
                Console.WriteLine("Constructor");
                field = InitField();
            }

            private static int InitStaticField()
            {
                Console.WriteLine("Initializin static field");
                return 0;
            }

            private int InitField()
            {
                Console.WriteLine("Initializing field");
                return 0;
            }
        }

    }

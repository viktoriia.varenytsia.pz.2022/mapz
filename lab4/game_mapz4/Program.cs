﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using System.Windows.Forms;

//namespace game_mapz4
//{


//    internal static class Program
//    {
//        /// <summary>
//        /// The main entry point for the application.
//        /// </summary>
//        [STAThread]
//        static void Main()
//        {
//            Application.EnableVisualStyles();
//            Application.SetCompatibleTextRenderingDefault(false);
//            Application.Run(new Form1());
//        }
//    }
    
//}


//// Клас для представлення героя
//public class Hero
//        {
//            public string Name { get; set; }
//            public int Health { get; set; }
//            public int Level { get; set; }
//            public int Gold { get; set; } // Золото героя
//            public int Strength { get; set; } // Сила героя
//            public int Magic { get; set; } // Магія героя
//            public int Economy { get; set; } // Економіка героя
//            public List<Item> Inventory { get; set; } // Інвентар героя

//            public Hero(string name)
//            {
//                Name = name;
//                Health = 100; // Початкове здоров'я героя
//                Level = 1; // Початковий рівень героя
//                Gold = 0; // Початкова кількість золота
//                Strength = 1; // Початковий рівень сили
//                Magic = 1; // Початковий рівень магії
//                Economy = 1; // Початковий рівень економіки
//                Inventory = new List<Item>(); // Ініціалізуємо інвентар героя
//            }

//            public void GatherResource(int amount)
//            {
//                Gold += amount; // Додаємо зібрані ресурси до золота героя
//            }

//            public void UpgradeStrength()
//            {
//                // Збільшуємо силу героя за вартістю
//                if (Gold >= 50) // Наприклад, вартість апгрейду сили
//                {
//                    Gold -= 50; // Віднімаємо вартість апгрейду зі золота героя
//                    Strength++; // Збільшуємо силу героя
//                    Console.WriteLine($"Герой {Name} вдосконалив свою силу");
//                }
//                else
//                {
//                    Console.WriteLine($"Не вистачає золота для вдосконалення сили героя");
//                }
//            }

//            public void UpgradeMagic()
//            {
//                // Збільшуємо магію героя за вартістю
//                if (Gold >= 50) // Наприклад, вартість апгрейду магії
//                {
//                    Gold -= 50; // Віднімаємо вартість апгрейду зі золота героя
//                    Magic++; // Збільшуємо магію героя
//                    Console.WriteLine($"Герой {Name} вдосконалив свою магію");
//                }
//                else
//                {
//                    Console.WriteLine($"Не вистачає золота для вдосконалення магії героя");
//                }
//            }

//            public void UpgradeEconomy()
//            {
//                // Збільшуємо економіку героя за вартістю
//                if (Gold >= 50) // Наприклад, вартість апгрейду економіки
//                {
//                    Gold -= 50; // Віднімаємо вартість апгрейду зі золота героя
//                    Economy++; // Збільшуємо економіку героя
//                    Console.WriteLine($"Герой {Name} вдосконалив свою економіку");
//                }
//                else
//                {
//                    Console.WriteLine($"Не вистачає золота для вдосконалення економіки героя");
//                }
//            }

//            public void BuyItem(Item item)
//            {
//                if (Gold >= item.Price) // Перевіряємо, чи герой може купити предмет
//                {
//                    Gold -= item.Price; // Віднімаємо вартість предмету зі золота героя
//                    Inventory.Add(item); // Додаємо предмет до інвентаря героя
//                    Console.WriteLine($"Герой {Name} придбав {item.Name}");
//                }
//                else
//                {
//                    Console.WriteLine($"Не вистачає золота для покупки {item.Name}");
//                }
//            }
//        }

//        // Клас для представлення предметів
//        public class Item
//        {
//            public string Name { get; set; }
//            public int Price { get; set; }

//            public Item(string name, int price)
//            {
//                Name = name;
//                Price = price;
//            }
//        }

//        // Клас для магазину предметів
//        public class Shop
//        {
//            private List<Item> items; // Список доступних предметів в магазині

//            public Shop()
//            {
//                items = new List<Item>(); // Ініціалізуємо список предметів
//            }

//            public void AddItem(Item item)
//            {
//                items.Add(item); // Додаємо предмет до списку доступних у магазині
//            }

//            public void ShowItems()
//            {
//                Console.WriteLine("Товари в магазині:");
//                foreach (var item in items)
//                {
//                    Console.WriteLine($"{item.Name} - Ціна: {item.Price}");
//                }
//            }
//        }

//        // Приклад використання
//        partial class Form1 : Form
//        {
//            private Hero hero;
//            private Shop shop;

//            public Form1()
//            {
//                InitializeComponent();
//                InitializeGame();
//            }

   

//    private void InitializeGame()
//            {
//                hero = new Hero("Player"); // Створення героя
//                shop = new Shop(); // Створення магазину
//                InitializeShop(); // Ініціалізація товарів у магазині
//            }

//            private void InitializeShop()
//            {
//                // Додавання предметів у магазин
//                shop.AddItem(new Item("Sword", 50));
//                shop.AddItem(new Item("Shield", 30));
//                shop.AddItem(new Item("Potion", 20));
//            }

//            // Обробник кліку на кнопку "Start game"
//            private void btnStartGame_Click(object sender, EventArgs e)
//            {
//                // Початок гри
//                MessageBox.Show("Гра почалася!");
//            }

//            // Обробник кліку на кнопку "Upgrade"
//            private void btnUpgradeSkills_Click(object sender, EventArgs e)
//            {
//                // Вдосконалення навичок героя
//                hero.UpgradeStrength(); // Приклад виклику методу для вдосконалення сили героя
//                hero.UpgradeMagic(); // Приклад виклику методу для вдосконалення магії героя
//                hero.UpgradeEconomy(); // Приклад виклику методу для вдосконалення економіки героя
//            }

//            // Обробник кліку на кнопку "Buy equipment"
//            private void btnBuyEquipment_Click(object sender, EventArgs e)
//            {
//                // Показуємо товари у магазині та дозволяємо гравцю купувати предмети
//                shop.ShowItems();
//                // Наприклад, при натисканні на кнопку гравець може купити предмет і додати його до інвентаря героя
//                hero.BuyItem(new Item("Sword", 50));
//            }
//        }
    
// game.service.ts
import { Injectable } from '@angular/core';
import { Hero } from '../game/hero/hero.interface';
import { Item } from '../game/item/item.interface';
import { Skill } from '../game/skill/skill.interface';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  hero: Hero;

  constructor() {
    this.hero = {
      name: 'Hero',
      level: 1,
      experience: 0,
      health: 100,
      strength: 10,
      magic: 10,
      gold: 100,
      skills: ['Basic Attack'],
      inventory: ['Health Potion'],
      posX: 0,
      posY: 0,
    };
  }

  levelUp() {
    this.hero.level++;
    this.hero.strength += 5;
    this.hero.magic += 5;
    this.hero.health += 20;
  }

  gainExperience(exp: number) {
    this.hero.experience += exp;
    if (this.hero.experience >= 100) {
      this.levelUp();
      this.hero.experience -= 100;
    }
  }

  buyItem(item: Item) {
    if (this.hero.gold >= item.price) {
      this.hero.gold -= item.price;
      this.hero.inventory.push(item.name);
    }
  }

  buySkill(skill: Skill) {
    if (
      this.hero.gold >= skill.price &&
      !this.hero.skills.includes(skill.name)
    ) {
      this.hero.gold -= skill.price;
      this.hero.skills.push(skill.name);
    }
  }

  usePotion() {
    const potionIndex = this.hero.inventory.indexOf('Health Potion');
    if (potionIndex !== -1) {
      this.hero.health += 50;
      this.hero.inventory.splice(potionIndex, 1);
    }
  }
}

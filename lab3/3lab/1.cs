﻿using System;

public class Plant
{
    public Guid Id { get; } = Guid.NewGuid();
    public string Name { get; set; }
    public string Species { get; set; }
    public bool IsFlowering { get; set; }

    public Plant(string name, string species, bool isFlowering = false)
    {
        Name = name;
        Species = species;
        IsFlowering = isFlowering;
    }

    public string SelectInformation()
    {
        return $"Назва: {Name}, Вид: {Species}, Цвітіння: {IsFlowering}";
    }
}

public class Species
{
    public Guid Id { get; } = Guid.NewGuid();
    public string Genus { get; set; }
    public string SpeciesName { get; set; }
    public bool IsEndangered { get; set; }
    public Species(string genus, string speciesName, bool isEndangered = false)
    {
        Genus = genus;
        SpeciesName = speciesName;
        IsEndangered = isEndangered;
    }

    public string SelectInformation()
    {
        return $"Рід: {Genus}, Назва виду: {SpeciesName}, На межі вимирання: {IsEndangered}";
    }
}

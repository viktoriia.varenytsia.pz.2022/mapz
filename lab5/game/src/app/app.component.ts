import { Skill } from '../game/skill/skill.interface';
import { Component } from '@angular/core';
import { GameService } from './game.service';
import { Item, ItemType } from '../game/item/item.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  standalone: true,
})
export class AppComponent {
  constructor(public gameService: GameService) {}

  items: Item[] = [
    { name: 'Sword', type: ItemType.Weapon, effect: 10, price: 50 },
    { name: 'Shield', type: ItemType.Armor, effect: 20, price: 75 },
    { name: 'Health Potion', type: ItemType.Potion, effect: 50, price: 25 },
  ];

  skills: Skill[] = [
    {
      name: 'Fireball',
      description: 'Launches a fireball',
      effect: 20,
      price: 100,
    },
    { name: 'Heal', description: 'Heals the hero', effect: 30, price: 150 },
  ];
}

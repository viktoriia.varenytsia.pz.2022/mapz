import { Component, HostListener } from '@angular/core';
import { Hero } from './hero.interface';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css'],
})
export class HeroComponent {
  hero: Hero;

  constructor() {
    this.hero = {
      name: 'Hero',
      level: 1,
      experience: 0,
      health: 100,
      strength: 10,
      magic: 10,
      gold: 100,
      skills: ['Basic Attack'],
      inventory: ['Health Potion'],
      posX: 0,
      posY: 10,
    };
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    const step = 10; // Визначте крок переміщення
    switch (event.key) {
      case 'ArrowUp':
        this.hero.posY -= step;
        break;
      case 'ArrowDown':
        this.hero.posY += step;
        break;
      case 'ArrowLeft':
        this.hero.posX -= step;
        break;
      case 'ArrowRight':
        this.hero.posX += step;
        break;
      default:
        break;
    }
  }
}

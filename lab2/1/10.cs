﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    public class Person
    {
        public string name;
        public int age;
        public string gender;

        public Person()
        {
            name = "Unknown";
            age = 0;
            gender = "Unknown";
        }

        public Person(string name, int age, string gender)
        {
            this.name = name;
            this.age = age;
            this.gender = gender;
        }

        public void Display()
        {
            Console.WriteLine("Name: {0}", name);
            Console.WriteLine("Age: {0}", age);
            Console.WriteLine("Gender: {0}", gender);
        }
    }

    public class Student : Person
    {
        public int studentId;

        // Перевантаження з використанням base
        public Student() : base()
        {
            studentId = 0;
        }

        // Перевантаження з використанням base
        public Student(string name, int age, string gender, int studentId) : base(name, age, gender)
        {
            // Використання this для доступу до поля
            this.studentId = studentId;
        }

        // Перевантаження з використанням this
        public Student(string name, int age, string gender) : this(name, age, gender, 0)
        {
        }

        public void Display()
        {
            base.Display();
            Console.WriteLine("Student ID: {0}", studentId);
        }
    }

}

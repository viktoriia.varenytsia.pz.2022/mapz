export interface Hero {
  name: string;
  level: number;
  experience: number;
  health: number;
  strength: number;
  magic: number;
  gold: number;
  skills: string[];
  inventory: string[];
  posX: number; // Додайте координату X
  posY: number; // Додайте координату Y
}

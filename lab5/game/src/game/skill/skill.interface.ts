// skill.interface.ts

// Інтерфейс для представлення навички
export interface Skill {
  name: string; // Назва навички
  description: string; // Опис навички
  effect: number; // Ефект, який надає навичка
  price: number; // Ціна навички
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace game_mapz4
{
    public partial class Form1 : Form
    {
        private System.ComponentModel.IContainer components = null;

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnStartGame = new System.Windows.Forms.Button();
            this.btnUpgradeSkills = new System.Windows.Forms.Button();
            this.btnBuyEquipment = new System.Windows.Forms.Button();
            this.lblPlayerName = new System.Windows.Forms.Label();
            this.lblPlayerHealth = new System.Windows.Forms.Label();
            this.lblLevel = new System.Windows.Forms.Label();
            this.picPlayerAvatar = new System.Windows.Forms.PictureBox();
            this.picEquipment = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picPlayerAvatar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEquipment)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStartGame
            // 
            this.btnStartGame.Location = new System.Drawing.Point(13, 13);
            this.btnStartGame.Name = "btnStartGame";
            this.btnStartGame.Size = new System.Drawing.Size(75, 23);
            this.btnStartGame.TabIndex = 0;
            this.btnStartGame.Text = "Start game";
            this.btnStartGame.UseVisualStyleBackColor = true;
            // 
            // btnUpgradeSkills
            // 
            this.btnUpgradeSkills.Location = new System.Drawing.Point(95, 13);
            this.btnUpgradeSkills.Name = "btnUpgradeSkills";
            this.btnUpgradeSkills.Size = new System.Drawing.Size(75, 23);
            this.btnUpgradeSkills.TabIndex = 1;
            this.btnUpgradeSkills.Text = "Upgrade";
            this.btnUpgradeSkills.UseVisualStyleBackColor = true;
            // 
            // btnBuyEquipment
            // 
            this.btnBuyEquipment.Location = new System.Drawing.Point(177, 12);
            this.btnBuyEquipment.Name = "btnBuyEquipment";
            this.btnBuyEquipment.Size = new System.Drawing.Size(75, 23);
            this.btnBuyEquipment.TabIndex = 2;
            this.btnBuyEquipment.Text = "Buy equipment";
            this.btnBuyEquipment.UseVisualStyleBackColor = true;
            // 
            // lblPlayerName
            // 
            this.lblPlayerName.AutoSize = true;
            this.lblPlayerName.Location = new System.Drawing.Point(359, 19);
            this.lblPlayerName.Name = "lblPlayerName";
            this.lblPlayerName.Size = new System.Drawing.Size(44, 16);
            this.lblPlayerName.TabIndex = 3;
            this.lblPlayerName.Text = "Name";
            // 
            // lblPlayerHealth
            // 
            this.lblPlayerHealth.AutoSize = true;
            this.lblPlayerHealth.Location = new System.Drawing.Point(423, 19);
            this.lblPlayerHealth.Name = "lblPlayerHealth";
            this.lblPlayerHealth.Size = new System.Drawing.Size(46, 16);
            this.lblPlayerHealth.TabIndex = 4;
            this.lblPlayerHealth.Text = "Health";
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Location = new System.Drawing.Point(487, 19);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(40, 16);
            this.lblLevel.TabIndex = 5;
            this.lblLevel.Text = "Level";
            // 
            // picPlayerAvatar
            // 
            this.picPlayerAvatar.Image = ((System.Drawing.Image)(resources.GetObject("picPlayerAvatar.Image")));
            this.picPlayerAvatar.Location = new System.Drawing.Point(75, 200);
            this.picPlayerAvatar.Name = "picPlayerAvatar";
            this.picPlayerAvatar.Size = new System.Drawing.Size(79, 135);
            this.picPlayerAvatar.TabIndex = 6;
            this.picPlayerAvatar.TabStop = false;
            // 
            // picEquipment
            // 
            this.picEquipment.Image = ((System.Drawing.Image)(resources.GetObject("picEquipment.Image")));
            this.picEquipment.Location = new System.Drawing.Point(138, 233);
            this.picEquipment.Name = "picEquipment";
            this.picEquipment.Size = new System.Drawing.Size(83, 39);
            this.picEquipment.TabIndex = 7;
            this.picEquipment.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.picEquipment);
            this.Controls.Add(this.picPlayerAvatar);
            this.Controls.Add(this.lblLevel);
            this.Controls.Add(this.lblPlayerHealth);
            this.Controls.Add(this.lblPlayerName);
            this.Controls.Add(this.btnBuyEquipment);
            this.Controls.Add(this.btnUpgradeSkills);
            this.Controls.Add(this.btnStartGame);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picPlayerAvatar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEquipment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Button btnStartGame;
        private System.Windows.Forms.Button btnUpgradeSkills;
        private System.Windows.Forms.Button btnBuyEquipment;
        private System.Windows.Forms.Label lblPlayerName;
        private System.Windows.Forms.Label lblPlayerHealth;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.PictureBox picPlayerAvatar;
        private System.Windows.Forms.PictureBox picEquipment;

        public class Hero
        {
            public string Name { get; set; }
            public int Health { get; set; }
            public int Level { get; set; }
            public int Gold { get; set; }
            public int Strength { get; set; }
            public int Magic { get; set; }
            public int Dexterity { get; set; }
            public int Luck { get; set; }
            public List<Item> Inventory { get; set; }
            public int Economy { get; private set; }

            public void GatherResource(int amount)
            {
                Gold += amount;
            }

            public void UpgradeStrength()
            {
                if (Gold >= 50)
                {
                    Gold -= 50;
                    Strength++;
                    Console.WriteLine($"Hero {Name} upgraded strength");
                }
                else
                {
                    Console.WriteLine($"Not enough gold to upgrade strength for hero {Name}");
                }
            }

            public void UpgradeMagic()
            {
                if (Gold >= 50)
                {
                    Gold -= 50;
                    Magic++;
                    Console.WriteLine($"Hero {Name} upgraded magic");
                }
                else
                {
                    Console.WriteLine($"Not enough gold to upgrade magic for hero {Name}");
                }
            }

            public void UpgradeEconomy()
            {
                if (Gold >= 50)
                {
                    Gold -= 50;
                    Economy++;
                    Console.WriteLine($"Hero {Name} upgraded economy");
                }
                else
                {
                    Console.WriteLine($"Not enough gold to upgrade economy for hero {Name}");
                }
            }

            public void BuyItem(Item item)
            {
                if (Gold >= item.Price)
                {
                    Gold -= item.Price;
                    Inventory.Add(item);
                    Console.WriteLine($"Hero {Name} bought {item.Name}");
                }
                else
                {
                    Console.WriteLine($"Not enough gold to buy {item.Name}");
                }
            }
        }

        public class Item
        {
            public string Name { get; set; }
            public int Price { get; set; }

            public Item(string name, int price)
            {
                Name = name;
                Price = price;
            }
        }

        public class Shop
        {
            private List<Item> items;

            public Shop()
            {
                items = new List<Item>();
            }

            public void AddItem(Item item)
            {
                items.Add(item);
            }

            public void ShowItems()
            {
                Console.WriteLine("Items in shop:");
                foreach (var item in items)
                {
                    Console.WriteLine($"{item.Name} - Price: {item.Price}");
                }
            }
        }
    }

    internal static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}

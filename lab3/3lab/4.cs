﻿using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;


public class PlantTests
{
    [Fact]
    public void PlantListToSortedListByCost_PlantSortedListFirstElementCost_ShouldBeEqualToMinimumCost()
    {
        // Arrange
        var plants = new List<Plant>(PlantMock.Plants);
        const int MINIMUM_COST = 18000;
        // Act
        var plantSortedListFirstElementCost = plants.OrderBy(plant => plant.Cost).First().Cost;
        // Assert
        Assert.Equal(MINIMUM_COST, plantSortedListFirstElementCost);
    }
    [Fact]
    public void EnqueueAndDequeue_FirstEnqueuedPlant_ShouldBeFirstDequeued()
    {
        // Arrange
        var queue = new Queue<Plant>();
        var expectedDequeuedPlant = new Plant("TestName", "TestSpecies", 29000, "Red");
        var plantsToEnqueue = PlantMock.Plants;
        // Act
        queue.Enqueue(expectedDequeuedPlant);
        plantsToEnqueue.ForEach(plant => queue.Enqueue(plant));
        var actualDequeuedPlant = queue.Dequeue();
        // Assert
        Assert.Equal(expectedDequeuedPlant, actualDequeuedPlant);
    }


    [Fact]
    public void PushAndPop_LastPushedPlant_ShouldBeFirstPopped()
    {
        // Arrange
        var plantStack = new Stack<Plant>(PlantMock.Plants);
        var expectedPoppedPlant = new Plant("TestName", "TestSpecies", 29000, "Red");
        // Act
        plantStack.Push(expectedPoppedPlant);
        var actualPoppedPlant = plantStack.Pop();
        // Assert
        Assert.Equal(expectedPoppedPlant, actualPoppedPlant);
    }


    [Fact]
    public void HashSetAddNonUniqueElement_HashSetAmount_ShouldBeEqualToExpected()
    {
        // Arrange
        var plantHashSet = new HashSet<Plant>(PlantMock.Plants);
        var expectedHashSetAmount = plantHashSet.Count();
        var nonUniquePlantToAdd = plantHashSet.ElementAt(new Random().Next(expectedHashSetAmount));
        // Act
        plantHashSet.Add(nonUniquePlantToAdd);
        var actualHashSetAmount = plantHashSet.Count();
        // Assert
        Assert.Equal(expectedHashSetAmount, actualHashSetAmount);
    }


    [Fact]
    public void Lookup_ReturnsCorrectPlants()
    {
        // Arrange
        var lookup = PlantMock.Plants.ToLookup(plant => plant.Cost);
        const int MULTIPLE_PLANTS_COST = 20000;
        const int EXPECTED_COUNT_OF_PLANTS = 2;
        // Act
        var actualCountOfPlants = lookup[MULTIPLE_PLANTS_COST].Count();
        // Assert
        Assert.Equal(EXPECTED_COUNT_OF_PLANTS, actualCountOfPlants);
    }
}





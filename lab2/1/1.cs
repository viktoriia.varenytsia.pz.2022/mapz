﻿using System;

namespace lab2
{
    public interface ISleep
    {
        void Sleep();
    }

    public abstract class Animal
    {
        public string Name { get; set; }

        public Animal(string name)
        {
            Name = name;
        }

        public abstract void MakeSound();
    }

    public class Cat : Animal, ISleep
    {
        public Cat(string name) : base(name) { }

        public override void MakeSound()
        {
            Console.WriteLine("Meow!");
        }

        public void Sleep()
        {
            Console.WriteLine("Cat sleeping");
        }
    }
}

﻿using System;

namespace lab2.1
{
    public class Animal
    {
        // Публічне поле
        public string PublicField = "I am public";

        // Приватне поле
        private string privateField = "I am private";

        // Захищене поле
        protected string protectedField = "I am protected";

        // Внутрішнє поле
        internal string internalField = "I am internal";

        // Захищене внутрішнє поле
        protected internal string protectedInternalField = "I am protected internal";

        // Закритий (private) метод
        private void PrivateMethod()
        {
            Console.WriteLine("Private Method");
        }

        // Відкритий (public) метод
        public void PublicMethod()
        {
            Console.WriteLine("Public Method");
        }

        // Захищений (protected) метод
        protected void ProtectedMethod()
        {
            Console.WriteLine("Protected Method");
        }

        // Внутрішній (internal) метод
        internal void InternalMethod()
        {
            Console.WriteLine("Internal Method");
        }

        // Захищений внутрішній (protected internal) метод
        protected internal void ProtectedInternalMethod()
        {
            Console.WriteLine("Protected Internal Method");
        }

        // Метод, який викликає різні методи та властивості
        public void DisplayInfo()
        {
            Console.WriteLine($"Public Field: {PublicField}");
            Console.WriteLine($"Private Field: {privateField}");
            Console.WriteLine($"Protected Field: {protectedField}");
            Console.WriteLine($"Internal Field: {internalField}");
            Console.WriteLine($"Protected Internal Field: {protectedInternalField}");

            // Виклик приватного методу
            PrivateMethod();

            // Виклик відкритого методу
            PublicMethod();

            // Виклик захищеного методу
            ProtectedMethod();

            // Виклик внутрішнього методу
            InternalMethod();

            // Виклик захищеного внутрішнього методу
            ProtectedInternalMethod();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Створення об'єкта класу AnimalInfo
            Animal Animal = new Animal();

            Animal.PublicMethod();




            Animal.PrivateMethod();



            Animal.ProtectedMethod();






            Animal.InternalMethod();





            Animal.ProtectedInternalMethod();

            // Виклик методу DisplayInfo, який виводить інформацію про поля та методи класу
            animalInfo.DisplayInfo();
        }
    }
}

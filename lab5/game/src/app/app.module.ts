import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { ShopComponent } from '../app/item/shop/shop.components';
import { ItemService } from '../app/item/item.service';

@NgModule({
  declarations: [ShopComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
  ],
  providers: [ItemService],
  bootstrap: [ShopComponent],
})
export class AppModule {}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3lab
{
    [Fact]
    public void BoolTest_NonStandartPlant_ShouldBeFromOnlyOneGenus()
    {
        // Arrange
        var models = PlantMock.Models.OrderBy(model => model.Genus);

        // Act
        var firstIsTrue = models.FirstOrDefault(model => model.IsTraditional);


        // Assert
        Assert.True(firstIsTrue?.IsTraditional ?? false);
    }


}

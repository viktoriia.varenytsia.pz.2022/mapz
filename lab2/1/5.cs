﻿using System;

namespace lab2
{
    public enum CarModels
    {
        A4,
        A5,
        B4,
        B5
    }

    class Cars
    {
        static void Main()
        {
            CarModels myCar = CarModels.A4;

            if ((myCar & CarModels.B4) == CarModels.B4)
                Console.WriteLine("Car model is B4");
            else
                Console.WriteLine("Car model is not B4");

            if ((myCar | CarModels.A5) == CarModels.A5)
                Console.WriteLine("Car model is A5");
            else
                Console.WriteLine("Car model is not A5");

            if ((myCar ^ CarModels.B4) == CarModels.A4)
                Console.WriteLine("Car model is not B4");
            else
                Console.WriteLine("Car model is B4");

            if (myCar == CarModels.A4 && myCar != CarModels.B4)
                Console.WriteLine("Car model is A4 and not B4");
            else
                Console.WriteLine("Not meet the requirements");

            if (myCar == CarModels.A5 || myCar == CarModels.B5)
                Console.WriteLine("Car model is A5 or B5");
            else
                Console.WriteLine("Not meet the requirements");
        }
    }
}

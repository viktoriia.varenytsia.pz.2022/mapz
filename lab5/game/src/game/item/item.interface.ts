// item.interface.ts

// Інтерфейс для представлення предмету
export interface Item {
  name: string; // Назва предмету
  type: ItemType; // Тип предмету
  effect: number; // Ефект, який надає предмет
  price: number; // Ціна предмету
}

// Перерахування типів предметів
export enum ItemType {
  Weapon = 'weapon',
  Armor = 'armor',
  Potion = 'potion',
}
